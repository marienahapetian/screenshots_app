<?php

namespace App\Http\Controllers;

use App\Image;
use Illuminate\Http\Request;

class ImageController extends Controller
{
    public function index($code)
    {
        $image = $this->getImage($code);
        return view('page.image',compact('image'));
    }

    protected function getImage($code)
    {
        $image = Image::where('code',$code)->first();

        if($image) {
            $image = $image->image;
            return $image;
        }

        abort(404);
    }
}
